# Migrate Citavi 2 JabRef

This could be a simple (python or anything else) script helps you migrating your bibliographic data (including attachments and groups/categories) from Citavi to JabRef.

# Current status

There is no code actually. But if you really need to migrate a Citavi project to JabRef I can help you with this. There is a code base in the sibling project [Migrate JabRef 2 Citavi](https://codeberg.org/buhtz/Migrate_JabRef_2_Citavi) where you can see how the Citavi data structure works.
